import { Component, OnInit } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';
import { UrlService } from 'src/app/provider/url.service';
import { Http, RequestOptions } from '@angular/http';
import { map } from 'rxjs/operators';
import { NavController } from '@ionic/angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-adicionar-foto',
  templateUrl: './adicionar-foto.page.html',
  styleUrls: ['./adicionar-foto.page.scss'],
})
export class AdicionarFotoPage implements OnInit {
  
  fotoAtual;
  novaFoto;
  codigo;

  constructor(private camera: Camera,
              public file: File,
              public http: Http,
              public navCtrl: NavController,
              public urlService: UrlService,
              public fileTrans: FileTransfer) {

   }


   tirarFoto(){
     const options: CameraOptions = {
       quality: 75,
       destinationType: this.camera.DestinationType.DATA_URL,
       encodingType: this.camera.EncodingType.JPEG,
       mediaType: this.camera.MediaType.PICTURE
     }

     this.camera.getPicture(options).then((imgeData) => {
      this.urlService.exibirLoading();
      this.novaFoto = 'data:image/jpeg;base64,'+imgeData;
      this.urlService.fecharLoding();
     }, error => {
       this.urlService.alertas('Atenção', 'Erro ao tentar capturar foto');
     });
   }

   buscarFoto(){
     const options: CameraOptions = {
        quality: 70,
        targetHeight: 640,
        targetWidth: 640,
        sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
        destinationType: this.camera.DestinationType.DATA_URL,
        mediaType: this.camera.MediaType.PICTURE,
        allowEdit: true
     };

     this.camera.getPicture(options).then((imageDate)=>{
        this.urlService.exibirLoading();
        this.novaFoto = 'data:image/jpeg;base64,'+imageDate;
        this.urlService.fecharLoding();
     }, (error) =>{
       this.urlService.alertas('Atenção', 'Algo deu errado!');
     });
   }

   salvarFoto(){

    let data = {
      'foto': this.novaFoto
  };


this.http.post(this.urlService.getUrl()+'uploadPhoto.php', data).pipe(map(resp => resp.json()))
      .subscribe(data => {
          console.log(data);
      })
}

   

  ngOnInit() {
  }

}
