import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeusProdutosPage } from './meus-produtos.page';

describe('MeusProdutosPage', () => {
  let component: MeusProdutosPage;
  let fixture: ComponentFixture<MeusProdutosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeusProdutosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeusProdutosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
